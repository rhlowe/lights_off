import { randomInt } from './helpers';

export default class Game
{
    constructor(size = 5)
    {
        this.size = size;
        this.turns = 0;

        this.start();
        this._watchDifficulty();
        this._registerResetButtonListener();
    }

    _activateRandomButtons()
    {
        const allButtons = Array.from(document.querySelectorAll('.switch'));

        for(let i = 0; i < allButtons.length; i++) {
            allButtons[randomInt(0, allButtons.length)].classList.toggle('active');
        }
    }


    _buildGameBoard(size)
    {
        console.info('Building Game Board');

        const gameBoard = document.querySelector('#app');

        for(let i = 0; i < size; i++) {
            const row = document.createElement('div');
            row.classList.add('row');

            for(let j = 0; j < size; j++) {
                const column = document.createElement('div');
                column.classList.add('column');
                row.appendChild(column);

                const button = document.createElement('a');
                button.classList.add('switch');
                button.href = '#';
                button.dataset.row = i;
                button.dataset.col = j;
                column.appendChild(button);
                this._registerButtonAction(button);
            }

            gameBoard.appendChild(row);
        }
    }

    _checkIfSolved()
    {
        if (!this._isUnsolved()) {
            this._removeGameEventListeners();

            const message = `Solved the puzzle in ${ this.turns} moves.`;
            const h2 = document.createElement('h2');
            h2.textContent = message;
            document.querySelector('.message').appendChild(h2);
        }
    }

    _getAdjacentElement(x, y)
    {
        try {
            return document.querySelector('#app').querySelectorAll('.row')[x].querySelectorAll('.column')[y].querySelector('a');
        } catch (error) {
            return null;
        }
    }

    _isUnsolved()
    {
        return Boolean(document.querySelectorAll('.active').length);
    }

    _registerButtonAction(element)
    {
        element.addEventListener('click', this._registerButtonActionEventListener.bind(this));
    }

    _registerButtonActionEventListener(event)
    {
        event.preventDefault();

        const col = event.srcElement.dataset.col;
        const row = event.srcElement.dataset.row;

        event.srcElement.classList.toggle('active');

        if (elementD = this._getAdjacentElement(+row + 1, +col)) {
            elementD.classList.toggle('active');
        }

        if (elementL = this._getAdjacentElement(+row, +col - 1)) {
            elementL.classList.toggle('active');
        }

        if (elementR = this._getAdjacentElement(+row, +col + 1)) {
            elementR.classList.toggle('active');
        }

        if (elementU = this._getAdjacentElement(+row - 1, +col)) {
            elementU.classList.toggle('active');
        }

        this._updateTurns(++this.turns);
        this._checkIfSolved();
    }

    _registerResetButtonListener()
    {
        document.querySelector('#restart').addEventListener('click', () => {
            this.reset();
        });
    }

    _removeGameEventListeners()
    {
        const allButtons = Array.from(document.querySelectorAll('.switch'));

        allButtons.forEach(item => {
            item.removeEventListener('click', this._registerButtonActionEventListener.bind(this));
        });

        Array.from(document.querySelectorAll('.row')).forEach(item => item.remove());
    }

    reset()
    {
        this._removeGameEventListeners();

        if (document.querySelector('.message').childNodes.length) {
            document.querySelector('.message').removeChild(document.querySelector('.message h2'));
        }

        this.start();
    }

    solve()
    {
        const binaryMaxNumber = binaryMaxString(this.size);
        const firstRowPatterns = [];
        const maxNumber = parseInt(binaryMaxNumber, 2);

        for(let i = 1; i <= maxNumber; i++) {
            firstRowPatterns.push( padString(Number(i).toString(2), '0', this.size) );
        }

        // TEMP
        // while (this._isUnsolved()) {}

        const rows = Array.from(document.querySelectorAll('#app .row'));

        for (let h = 0; h < rows.length-1; h++) {
            const row = Array.from(document.querySelectorAll('#app .row')[h].querySelectorAll('a'));

            for(let i = 0; i < row.length; i++) {
                if (row[i].classList.contains('active')) {
                    row[i].parentNode.parentNode.nextSibling.children[i].querySelector('a').click();
                }
            }
        }

        if (this._isUnsolved()) {
            let indicies = firstRowPatterns.shift().split('');

            for(let i = 0; i < indicies.length; i++) {
                if(+indicies[i]) {
                    document.querySelectorAll('#app .row')[0].querySelectorAll('a')[i].click();
                }
            }
        }
        // TEMP

        function binaryMaxString(size) {
            let output = '';
            for(let i = 0; i < size; i++) {
                output += '1';
            }
            return output;
        }

        function padString(srcString, padString, size) {
            while (srcString.length < size) {
                srcString = padString + srcString;
            }

            return srcString;
        }
    }

    start()
    {
        this._updateTurns(this.turns = 0);
        this._buildGameBoard(this.size);
        this._activateRandomButtons();
    }

    _updateTurns(value)
    {
        document.querySelector('#turns span').textContent = value;
    }

    _watchDifficulty()
    {
        const difficultyEl = document.querySelector('#difficulty');

        difficultyEl.addEventListener('change', this._watchDifficultyEventListener.bind(this));
    }

    _watchDifficultyEventListener(event)
    {
        this.size = event.srcElement.value;

        console.info(`Game difficulty: ${ event.srcElement.value }`);

        this.reset();
    }
}
