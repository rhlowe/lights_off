const babel      = require('rollup-plugin-babel');
const gulp       = require('gulp');
const rollup     = require('gulp-rollup');
const sass       = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const webserver  = require('gulp-webserver');

gulp.task('default', ['sass', 'babel', 'serve']);

/**
 * Run the webserver at http://localhost:1997
 * Should automatically open in the default browser and run Livereload
 */
gulp.task('serve', ['sass', 'babel'], () => {
    console.info('Serving site at http://localhost:1997');

    return gulp.src('./app')
        .pipe(webserver({
            livereload: true,
            open: true,
            port: 1997
        }))
    ;
});

/**
 * Babelify and watch the js
 */
gulp.task('babel', [], () => {
    console.info('Transpiling src files via Babel & Rollup');

    return gulp.src('./src/js/index.js')
        .pipe(rollup({
            sourceMap: true,
            plugins: [ babel() ],
        }))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest('./app/dist'))
    ;
});

gulp.watch('./src/js/**/*.js', ['babel']);

/**
 * Sass CSS compiling and watch
 */
gulp.task('sass', [], () => {
    console.info('Compiling SCSS into CSS');

    return gulp.src('./src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./app/dist'))
    ;
});

gulp.watch('./src/scss/**/*.scss', ['sass']);
